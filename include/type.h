#ifndef __SDK_TYPE_H__
#define __SDK_TYPE_H__
#include <string>
#include <functional>

typedef std::function <void(const int code, const std::string msg)> ErrorCallback;

typedef std::function <void(const unsigned char* image, int width, int height, const char* userId, const char* userName, const char* room)> VideoFrameCallback;

typedef std::function <void(const char* userId, const char* userName, const char* room, int online)> StatusCallback;

typedef std::function <void(const char* userId, const char* userName, const char* room, const char* context, const char* time)> ChatMessageCallback;

//typedef void (*VideoFrameCallback)(unsigned int width, unsigned int height);
enum class VideoQuality {
	QQVGA,
	QVGA,
	VGA,
	HD,
	FULLHD
};
#endif