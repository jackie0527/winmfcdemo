#ifndef __SDK_H__
#define __SDK_H__
#include <string>

#include "type.h"


#ifdef SDK_SHREADE_API
#define _API __declspec(dllexport)
#else
#define _API __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C"
{
#endif

	_API void sdk_shreade_init();

	_API void sdk_shreade_info(const std::string userId, const std::string userName, const std::string room);

	_API void sdk_shreade_uri(const std::string uri);

	_API void sdk_shreade_handler_error(ErrorCallback callback);

	_API void sdk_shreade_start();

	_API void sdk_shreade_close();

	_API void sdk_shreade_local(VideoFrameCallback callback);

	_API void sdk_shreade_remote(VideoFrameCallback callback);

	_API void sdk_shreade_srceen(VideoFrameCallback callback);

	_API void sdk_shreade_status(StatusCallback callback);

	_API void sdk_shreade_share(ErrorCallback callback);

	_API void sdk_shreade_share_status(StatusCallback callback);

	_API void sdk_shreade_share_stop();

	_API void sdk_shreade_chat_context(ChatMessageCallback callback);

	_API void sdk_shreade_chat_submit(const std::string msg);



#ifdef __cplusplus
}
#endif
#endif