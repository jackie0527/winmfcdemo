﻿
// DemoMain.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "WinMFCDemo.h"
#include "DemoMain.h"
#include "afxdialogex.h"

#include <random>
#include <regex>
#include <functional>

std::default_random_engine engine;
#define random(a,b) (engine()%(b-a)+a)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "sdk.h"
#include "array_size.h"


std::string Utf8ToGbk(const char* src_str)
{
	int len = MultiByteToWideChar(CP_UTF8, 0, src_str, -1, NULL, 0);
	wchar_t* wszGBK = new wchar_t[len + 1];
	memset(wszGBK, 0, len * 2 + 2);
	MultiByteToWideChar(CP_UTF8, 0, src_str, -1, wszGBK, len);
	len = WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, NULL, 0, NULL, NULL);
	char* szGBK = new char[len + 1];
	memset(szGBK, 0, len + 1);
	WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, szGBK, len, NULL, NULL);
	std::string strTemp(szGBK);
	if (wszGBK) delete[] wszGBK;
	if (szGBK) delete[] szGBK;
	return strTemp;
}

std::string GbkToUtf8(const char* src_str)
{
	int len = MultiByteToWideChar(CP_ACP, 0, src_str, -1, NULL, 0);
	wchar_t* wstr = new wchar_t[len + 1];
	memset(wstr, 0, len + 1);
	MultiByteToWideChar(CP_ACP, 0, src_str, -1, wstr, len);
	len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);
	char* str = new char[len + 1];
	memset(str, 0, len + 1);
	WideCharToMultiByte(CP_UTF8, 0, wstr, -1, str, len, NULL, NULL);
	std::string strTemp = str;
	if (wstr) delete[] wstr;
	if (str) delete[] str;
	return strTemp;
}



// DemoMain 对话框
DemoMain::DemoMain(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DEMO_MAIN, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void DemoMain::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(DemoMain, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SEND, &DemoMain::OnBnClickedButtonSend)
	ON_BN_CLICKED(IDC_BUTTON_SHARE, &DemoMain::OnBnClickedButtonShare)
	ON_MESSAGE(WM_UPDATE_STATIC, &DemoMain::OnUpdateStatic)
END_MESSAGE_MAP()


// DemoMain 消息处理程序

BOOL DemoMain::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标


	// TODO: 在此添加额外的初始化代码

	GetDlgItem(IDC_STATIC_SCREEN)->ShowWindow(FALSE);

	this->ShowWindow(SW_HIDE);
	INT_PTR nResponse = _login.DoModal();
	_name = _login._name;
	_room = _login._room;
	_uri = _login._uri;
	_argee = _login._argee;
	_uid.Format("%d", random(1000000, 9000000));


	if (_name.GetLength() <= 0 || _room.GetLength() <= 0 || _uri.GetLength() <= 0 || !_argee)
	{
		EndDialog(0);
	}

	if (nResponse == IDOK)
	{
		// TODO: 在此放置处理何时用
		//  “确定”来关闭对话框的代码
		//MessageBox("ok");
		this->ShowWindow(SW_SHOW);

	}
	else if (nResponse == IDCANCEL)
	{
		//MessageBox("cancel");
		// TODO: 在此放置处理何时用
		//  “取消”来关闭对话框的代码
		EndDialog(0);
		//this->ShowWindow(SW_SHOW);
	}
	SetWindowText(_name + "-" + _room);
	ShowWindow(SW_MAXIMIZE);


	ZeroMemory(&bmiScreen, sizeof(bmiScreen));
	bmiScreen.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmiScreen.bmiHeader.biPlanes = 1;
	bmiScreen.bmiHeader.biBitCount = 32;
	bmiScreen.bmiHeader.biCompression = BI_RGB;

	auto title = GetDlgItem(IDC_STATIC_TITLE);
	std::string hello = _name + ", 欢迎来到 " + _room + " 的房间";
	title->SetWindowTextA(LPCTSTR(hello.c_str()));


	auto shareButton = GetDlgItem(IDC_BUTTON_SHARE);
	isShare = false;

	auto chatList = (CListBox*)GetDlgItem(IDC_LIST_CHAT);
	chatList->SetHorizontalExtent(200);
	auto chatMsg = GetDlgItem(IDC_EDIT_MSG);
	auto chatSend = GetDlgItem(IDC_BUTTON_SEND);


	//std::string uri = "local.shreade.cn/wss";
	// TODO: 在此添加额外的初始化代码

	//wnd_ = GetDlgItem(IDC_STATIC_PIC1)->GetSafeHwnd();
	localSize = false;
	remoteSize = false;

	std::string userId = _uid;
	std::string userName = _name;
	std::string room = _room;
	std::string uri = _uri;

	sdk_shreade_init();
	sdk_shreade_info(userId, userName, room);
	sdk_shreade_uri(uri);
	sdk_shreade_local(std::bind(&DemoMain::localCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6));
	sdk_shreade_remote(std::bind(&DemoMain::remoteCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6));
	sdk_shreade_srceen(std::bind(&DemoMain::screenCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6));
	sdk_shreade_status(std::bind(&DemoMain::statusCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
	sdk_shreade_chat_context(std::bind(&DemoMain::chatCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));
	sdk_shreade_start();
	_CrtDumpMemoryLeaks();


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void DemoMain::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR DemoMain::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}




LRESULT DemoMain::OnUpdateStatic(WPARAM wParam, LPARAM lParam)
{
	char* pStr0 = (char*)lParam; // OK

	//std::string userId(CW2A(cstr.GetString(), CP_UTF8));
	std::cout << "message: " << pStr0 << std::endl;
	CRect rectDlgChangeSize;
	GetClientRect(&rectDlgChangeSize);

	auto title = GetDlgItem(IDC_STATIC_TITLE);
	CRect chatRect;
	title->GetClientRect(&chatRect);

	int startLeft = ((rectDlgChangeSize.right - (chatRect.right - chatRect.left)) / 2) - (162 * (mapItem_.size() + 1));

	int splitWidth = rcRight + 4;

	if (wParam == 1)
	{

		int indexIdc;
		auto iterItem = mapItem_.find(pStr0);
		if (iterItem != mapItem_.end()) {
			indexIdc = mapItem_[pStr0];
			mapItem_.erase(iterItem);
		}


		auto iterBit = mapBit_.find(pStr0);
		if (iterBit != mapBit_.end()) {
			mapBit_.erase(iterBit);
		}

		//bun1idStatic_--;

		auto iterCtrl = mapCtrl_.find(pStr0);
		if (iterCtrl != mapCtrl_.end()) {
			mapCtrl_[pStr0]->DestroyWindow();
			delete iterCtrl->second;
			mapCtrl_.erase(iterCtrl);
		}

		HWND wnd = GetDlgItem(indexIdc)->GetSafeHwnd();

		HDC hdc = ::GetDC(wnd);
		if (hdc == NULL) {
			std::cout << "hdc null" << std::endl;
			return 0;
		}
		::ReleaseDC(wnd, hdc);

		int i = 1;
		auto iter = mapCtrl_.begin();
		while (iter != mapCtrl_.end()) {
			int left = i * splitWidth;
			iter->second->SetWindowPos(NULL, startLeft + left, 3, rcRight, rcBottom, SWP_NOZORDER);
			std::cout << "message new left: " << (startLeft + left) << " userId: " << iter->first << std::endl;
			iter++;
			i++;
		}

		std::cout << "message 1 size: " << mapItem_.size() << std::endl;
		return 0;
	}




	int i = 0;
	auto iter = mapCtrl_.begin();
	while (iter != mapCtrl_.end()) {
		int left = i * splitWidth;
		iter->second->SetWindowPos(NULL, startLeft + left, 3, rcRight, rcBottom, SWP_NOZORDER);
		//std::cout << "message new left: " << left << " userId: " << iter->first << std::endl;
		iter++;
		i++;
	}


	int index = bun1idStatic_ + 1;
	//int left = mapItem_.size() * 323 + 3;

	std::cout << "message 0 size: " << mapItem_.size() << std::endl;
	std::cout << "message left: " << (startLeft + mapItem_.size() * splitWidth) << std::endl;
	CStatic* sta = new CStatic();
	sta->Create(LPCTSTR(pStr0), WS_CHILD | WS_VISIBLE, CRect((startLeft + mapItem_.size() * splitWidth), 3, rcRight, rcBottom), this, index);
	//sta->EnableDynamicLayout(true);

	bun1idStatic_++;

	sta->SetWindowPos(NULL, (startLeft + mapItem_.size() * splitWidth), 3, rcRight, rcBottom, SWP_NOZORDER);

	//hdc = sta->GetDC()->GetSafeHdc();
	//std::cout << "local 1 call back wnd" << rc.left << "*" << rc.top << "====" << rc.right << " * " << rc.bottom << std::endl;
	mapCtrl_[pStr0] = sta;
	mapItem_[pStr0] = index;

	BITMAPINFO bmi;
	ZeroMemory(&bmi, sizeof(bmi));
	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;

	mapBit_[pStr0] = bmi;

	return 0;
}




void DemoMain::localCallback(const unsigned char* image, int width, int height, const char* userId, const char* userName, const char* room) {
	//std::cout << "1" << std::endl;
	//std::cout << "local call back dlg" << width << "*" << height << std::endl;

	if (height == 0 || width == 0) return;

	//SendMessage

	HDC hdc;
	BITMAPINFO bmi;

	if (mapItem_.find(userId) == mapItem_.end())
	{
		::SendMessage(this->GetSafeHwnd(), WM_UPDATE_STATIC, 0, (LPARAM)userId);
	}

	int index = mapItem_[userId];
	HWND wnd = GetDlgItem(index)->GetSafeHwnd();

	hdc = ::GetDC(wnd);
	if (hdc == NULL) {
		std::cout << "hdc null" << std::endl;
		return;
	}

	bmi = mapBit_[userId];


	bmi.bmiHeader.biWidth = width;
	bmi.bmiHeader.biHeight = -height;
	bmi.bmiHeader.biSizeImage = width * height * (bmi.bmiHeader.biBitCount >> 3);

	if (image != NULL) {
		//std::cout << "local call back dlg" << width << "*" << height << std::endl;
		//std::cout << "4" << std::endl;
		HDC dc_mem = ::CreateCompatibleDC(hdc);
		::SetStretchBltMode(dc_mem, HALFTONE);

		// Set the map mode so that the ratio will be maintained for us.
		HDC all_dc[] = { hdc, dc_mem };
		for (int i = 0; i < arraysize(all_dc); ++i) {
			SetMapMode(all_dc[i], MM_ISOTROPIC);
			SetWindowExtEx(all_dc[i], width, height, NULL);
			SetViewportExtEx(all_dc[i], rcRight, rcBottom, NULL);
		}

		HBITMAP bmp_mem = ::CreateCompatibleBitmap(hdc, rcRight, rcBottom);
		HGDIOBJ bmp_old = ::SelectObject(dc_mem, bmp_mem);

		POINT logical_area = { rcRight, rcBottom };
		DPtoLP(hdc, &logical_area, 1);

		HBRUSH brush = ::CreateSolidBrush(RGB(0, 0, 0));
		RECT logical_rect = { 0, 0, logical_area.x, logical_area.y };
		::FillRect(dc_mem, &logical_rect, brush);
		::DeleteObject(brush);

		int x = (logical_area.x / 2) - (width / 2);
		int y = (logical_area.y / 2) - (height / 2);

		StretchDIBits(dc_mem, x, y, width, height,
			0, 0, width, height, image, &(bmi), DIB_RGB_COLORS, SRCCOPY);

		BitBlt(hdc, 0, 0, logical_area.x, logical_area.y,
			dc_mem, 0, 0, SRCCOPY);

		// Cleanup.
		::SelectObject(dc_mem, bmp_old);
		::DeleteObject(bmp_mem);
		::DeleteDC(dc_mem);

	}
	//::ReleaseDC(wnd, hdc);
	//::InvalidateRect(wnd, NULL, TRUE);
	//ReleaseDC(hdc);
}

void DemoMain::remoteCallback(const unsigned char* image, int width, int height, const char* userId, const char* userName, const char* room) {
	//std::cout << "remote call back dlg" << std::endl;

	std::string share(userId);
	if (share == "share")
	{
		screenCallback(image, width, height, userId, userName, room);
		return;
	}

	if (height == 0 || width == 0) return;

	HDC hdc;
	BITMAPINFO bmi;

	if (mapItem_.find(userId) == mapItem_.end())
	{
		if (mapOfflineItem_.find(userId) == mapOfflineItem_.end())
		{
			::SendMessage(this->GetSafeHwnd(), WM_UPDATE_STATIC, 0, (LPARAM)userId);
		}
		else {
			return;
		}
	}

	int index = mapItem_[userId];
	HWND wnd = GetDlgItem(index)->GetSafeHwnd();

	hdc = ::GetDC(wnd);
	if (hdc == NULL) {
		std::cout << "remote hdc null" << std::endl;
		return;
	}

	bmi = mapBit_[userId];

	//std::cout << "local 2 call back wnd" << rc.left << "*" << rc.top << "====" << rc.right << " * " << rc.bottom << std::endl;
	bmi.bmiHeader.biWidth = width;
	bmi.bmiHeader.biHeight = -height;
	bmi.bmiHeader.biSizeImage = width * height * (bmi.bmiHeader.biBitCount >> 3);

	if (image != NULL) {

		//std::cout << "4" << std::endl;
		HDC dc_mem = ::CreateCompatibleDC(hdc);
		::SetStretchBltMode(dc_mem, HALFTONE);

		// Set the map mode so that the ratio will be maintained for us.
		HDC all_dc[] = { hdc, dc_mem };
		for (int i = 0; i < arraysize(all_dc); ++i) {
			SetMapMode(all_dc[i], MM_ISOTROPIC);
			SetWindowExtEx(all_dc[i], width, height, NULL);
			SetViewportExtEx(all_dc[i], rcRight, rcBottom, NULL);
		}

		HBITMAP bmp_mem = ::CreateCompatibleBitmap(hdc, rcRight, rcBottom);
		HGDIOBJ bmp_old = ::SelectObject(dc_mem, bmp_mem);

		POINT logical_area = { rcRight, rcBottom };
		DPtoLP(hdc, &logical_area, 1);

		HBRUSH brush = ::CreateSolidBrush(RGB(0, 0, 0));
		RECT logical_rect = { 0, 0, logical_area.x, logical_area.y };
		::FillRect(dc_mem, &logical_rect, brush);
		::DeleteObject(brush);

		int x = (logical_area.x / 2) - (width / 2);
		int y = (logical_area.y / 2) - (height / 2);

		StretchDIBits(dc_mem, x, y, width, height,
			0, 0, width, height, image, &(bmi), DIB_RGB_COLORS, SRCCOPY);

		BitBlt(hdc, 0, 0, logical_area.x, logical_area.y,
			dc_mem, 0, 0, SRCCOPY);

		// Cleanup.
		::SelectObject(dc_mem, bmp_old);
		::DeleteObject(bmp_mem);
		::DeleteDC(dc_mem);
	}

	//::ReleaseDC(wnd, hdc);
	//delete image;
}

void DemoMain::screenCallback(const unsigned char* image, int width, int height, const char* userId, const char* userName, const char* room)
{
	if (height == 0 || width == 0) return;

	HDC hdc;

	HWND wnd = GetDlgItem(IDC_STATIC_SCREEN)->GetSafeHwnd();

	hdc = ::GetDC(wnd);
	if (hdc == NULL) {
		std::cout << "screen hdc null" << std::endl;
		return;
	}

	GetDlgItem(IDC_STATIC_SCREEN)->ShowWindow(TRUE);


	CRect rectDlgChangeSize;
	GetClientRect(&rectDlgChangeSize);

	auto title = GetDlgItem(IDC_STATIC_TITLE);
	CRect chatRect;
	title->GetClientRect(&chatRect);

	int right;
	int bottom;
	int spaceWidth = rectDlgChangeSize.right - (chatRect.right - chatRect.left);

	//int startLeft = ((rectDlgChangeSize.right - (chatRect.right - chatRect.left)) / 2) - 1920 / 2;

	int startLeft = 0;
	int startTop = rcBottom + 3;

	if (width > height && spaceWidth > width)
	{
		right = width;
		bottom = height;
		startLeft = (spaceWidth - width) / 2;
		startTop = ((rectDlgChangeSize.bottom - height) / 3) + startTop;

	}
	else {

		right = spaceWidth - 60;

		startLeft = (spaceWidth - right) / 2;

		float scale = (height * 1.0) / width;
		bottom = right * scale;
	}

	std::cout << "screen 2 call back wnd" << startLeft << "*" << startTop << "====" << right << " * " << bottom << std::endl;

	//return;
	GetDlgItem(IDC_STATIC_SCREEN)->SetWindowPos(NULL, startLeft, startTop, right, bottom, SWP_NOZORDER);


	bmiScreen.bmiHeader.biWidth = width;
	bmiScreen.bmiHeader.biHeight = -height;
	bmiScreen.bmiHeader.biSizeImage = width * height * (bmiScreen.bmiHeader.biBitCount >> 3);

	if (image != NULL) {

		//std::cout << "4" << std::endl;
		HDC dc_mem = ::CreateCompatibleDC(hdc);
		::SetStretchBltMode(dc_mem, HALFTONE);

		// Set the map mode so that the ratio will be maintained for us.
		HDC all_dc[] = { hdc, dc_mem };
		for (int i = 0; i < arraysize(all_dc); ++i) {
			SetMapMode(all_dc[i], MM_ISOTROPIC);
			SetWindowExtEx(all_dc[i], width, height, NULL);
			SetViewportExtEx(all_dc[i], right, bottom, NULL);
		}

		HBITMAP bmp_mem = ::CreateCompatibleBitmap(hdc, right, bottom);
		HGDIOBJ bmp_old = ::SelectObject(dc_mem, bmp_mem);

		POINT logical_area = { right, bottom };
		DPtoLP(hdc, &logical_area, 1);

		HBRUSH brush = ::CreateSolidBrush(RGB(0, 0, 0));
		RECT logical_rect = { 0, 0, logical_area.x, logical_area.y };
		::FillRect(dc_mem, &logical_rect, brush);
		::DeleteObject(brush);

		int x = (logical_area.x / 2) - (width / 2);
		int y = (logical_area.y / 2) - (height / 2);

		StretchDIBits(dc_mem, x, y, width, height,
			0, 0, width, height, image, &(bmiScreen), DIB_RGB_COLORS, SRCCOPY);

		BitBlt(hdc, 0, 0, logical_area.x, logical_area.y,
			dc_mem, 0, 0, SRCCOPY);

		// Cleanup.
		::SelectObject(dc_mem, bmp_old);
		::DeleteObject(bmp_mem);
		::DeleteDC(dc_mem);
	}

}

void DemoMain::statusCallback(const char* userId, const char* userName, const char* room, int online)
{
	//offline
	if (online == 0)
	{
		//
		if (mapOfflineItem_.find(userId) == mapOfflineItem_.end()) {
			mapOfflineItem_[userId] = 0;
		}
		::SendMessage(this->GetSafeHwnd(), WM_UPDATE_STATIC, 1, (LPARAM)userId);

	}
	else {
		auto iter = mapOfflineItem_.find(userId);
		if (iter != mapOfflineItem_.end()) {
			mapOfflineItem_.erase(iter);
		}
	}

}

void DemoMain::shareCallback(const char* userId, const char* userName, const char* room, int online)
{
	auto chatList = (CListBox*)GetDlgItem(IDC_LIST_CHAT);

	std::string name(userName);

	chatList->InsertString(-1, (name + "分享屏幕").c_str());
}

void DemoMain::chatCallback(const char* userId, const char* userName, const char* room, const char* context, const char* time)
{
	std::cout << context << std::endl;
	auto chatList = (CListBox*)GetDlgItem(IDC_LIST_CHAT);
	std::string name(userName);
	std::string msg = Utf8ToGbk(context);
	std::regex ex("(<[/]*p>)");

	std::string r = std::regex_replace(msg, ex, "");

	chatList->InsertString(-1, (name + ": " + r).c_str());

}

void DemoMain::errorCallback(const int code, const std::string msg)
{
	//0
	//分享屏幕成功 
	auto shareButton = (CButton*)GetDlgItem(IDC_BUTTON_SHARE);
	if (code == 0)
	{
		isShare = true;
		shareButton->EnableWindow(1);
		shareButton->SetWindowTextA("关闭分享");
	}


	if (code > 0)
	{
		MessageBox(msg.c_str());
		isShare = false;
		shareButton->EnableWindow(1);
		shareButton->SetWindowTextA("分享屏幕");
	}
}

void DemoMain::OnBnClickedButtonSend()
{
	// TODO: 在此添加控件通知处理程序代码
	auto chatMsg = (CEdit*)GetDlgItem(IDC_EDIT_MSG);
	CString strContent;
	chatMsg->GetWindowText(strContent);
	std::string msg((LPCTSTR)strContent);
	sdk_shreade_chat_submit(GbkToUtf8(msg.c_str()));

	SetDlgItemText(chatMsg->GetDlgCtrlID(), "");
	auto chatList = (CListBox*)GetDlgItem(IDC_LIST_CHAT);

	std::string userName = _name;
	chatList->InsertString(-1, (userName + ": " + msg).c_str());
}


void DemoMain::OnBnClickedButtonShare()
{
	// TODO: 在此添加控件通知处理程序代码
	auto shareButton = (CButton*)GetDlgItem(IDC_BUTTON_SHARE);
	if (isShare)
	{
		//close
		sdk_shreade_share_stop();
		shareButton->SetWindowTextA("分享屏幕");
		isShare = false;
		GetDlgItem(IDC_STATIC_SCREEN)->ShowWindow(FALSE);

	}
	else {

		shareButton->SetWindowTextA("准备中...");
		shareButton->EnableWindow(0);
		sdk_shreade_share(std::bind(&DemoMain::errorCallback, this, std::placeholders::_1, std::placeholders::_2));
	}
}


void DemoMain::OnCancel()
{
	// TODO: 在此添加专用代码和/或调用基类

	sdk_shreade_close();
	auto iter = mapCtrl_.begin();
	while (iter != mapCtrl_.end()) {
		delete iter->second;
		iter++;
	}
	CDialogEx::OnCancel();
}
