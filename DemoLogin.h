﻿#pragma once


// DemoLogin 对话框

class DemoLogin : public CDialogEx
{
	DECLARE_DYNAMIC(DemoLogin)

public:
	DemoLogin(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DemoLogin();

	// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEMO_LOGIN };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditName();
	afx_msg void OnEnChangeEditRoom();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonSubmit();


	CString _name;
	CString _room;
	CString _uri;

	bool _argee;
};
