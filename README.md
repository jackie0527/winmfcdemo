![logo](https://shreade.cn/image/logo.png)
## Windows MFC DEMO
——————————————————————————————————
*  exe
    -  [x64/Release/WinMFCDemo.exe](/x64/Release/#)

*  服务端、web端 
    -  [https://gitee.com/jackie0527/web_build](https://gitee.com/jackie0527/web_build)
*  Android DEMO
    -  [https://gitee.com/jackie0527/android-demo](https://gitee.com/jackie0527/android-demo)

##  演示效果
<p>
<img src="https://shreade.cn/image/p1.png" alt="demo" width="800" />
</p>
<p>
<img src="https://shreade.cn/image/p2.jpg" alt="demo" width="800" />
</p>


## 联系方式

<img src="https://shreade.cn/image/weixin200.jpg" alt="avatar" />

[https://shreade.cn](https://shreade.cn)

提供低延迟、高质量的音视频通信服务，专注于视频会议的私有化部署。
