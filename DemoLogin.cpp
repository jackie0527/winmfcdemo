﻿// DemoLogin.cpp: 实现文件
//

#include "pch.h"
#include "WinMFCDemo.h"
#include "DemoLogin.h"
#include "afxdialogex.h"


// DemoLogin 对话框

IMPLEMENT_DYNAMIC(DemoLogin, CDialogEx)

DemoLogin::DemoLogin(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DEMO_LOGIN, pParent)
{
}

DemoLogin::~DemoLogin()
{
}

void DemoLogin::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DemoLogin, CDialogEx)
	ON_EN_CHANGE(IDC_EDIT_NAME, &DemoLogin::OnEnChangeEditName)
	ON_EN_CHANGE(IDC_EDIT_ROOM, &DemoLogin::OnEnChangeEditRoom)
	ON_BN_CLICKED(IDC_BUTTON_SUBMIT, &DemoLogin::OnBnClickedButtonSubmit)
END_MESSAGE_MAP()


// DemoLogin 消息处理程序

void DemoLogin::OnEnChangeEditName()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_NAME);
	CString strContent;
	pEdit->GetWindowText(strContent);
	if (strContent.GetLength() > 8) {
		SetDlgItemText(IDC_EDIT_NAME, strContent.Left(8));
		pEdit->SetSel(strContent.GetLength(), strContent.GetLength());//重设给光标设置位置 指向最后           
	}
}


void DemoLogin::OnEnChangeEditRoom()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_ROOM);
	CString strContent;
	pEdit->GetWindowText(strContent);
	if (strContent.GetLength() > 4) {
		SetDlgItemText(IDC_EDIT_ROOM, strContent.Left(4));
		pEdit->SetSel(strContent.GetLength(), strContent.GetLength());//重设给光标设置位置 指向最后           
	}
	else {
		int intNo = _tstoi(strContent);
		CString sNo;
		sNo.Format(_T("%d"), intNo);
		if (sNo != strContent)
		{
			SetDlgItemText(IDC_EDIT_ROOM, strContent);
		}

	}
}


BOOL DemoLogin::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	SetDlgItemText(IDC_EDIT_NAME, "li_win");
	SetDlgItemText(IDC_EDIT_ROOM, "5555");
	//SetDlgItemText(IDC_EDIT_URI, "wss://www.shreade.cn/wss");
	SetDlgItemText(IDC_EDIT_URI, "wss://local.shreade.cn/wss");

	((CButton*)GetDlgItem(IDC_RADIO_ARGEE))->SetCheck(TRUE);
	//_tprintf_s

	SetWindowText("Login");

	UpdateData(TRUE);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}


void DemoLogin::OnBnClickedButtonSubmit()
{
	// TODO: 在此添加控件通知处理程序代码

	_argee = ((CButton*)GetDlgItem(IDC_RADIO_ARGEE))->GetCheck();
	if (!_argee)
	{
		MessageBox("请选择同意");
	}

	CEdit* nameCtrl = (CEdit*)GetDlgItem(IDC_EDIT_NAME);
	nameCtrl->GetWindowText(_name);

	CEdit* roomCtrl = (CEdit*)GetDlgItem(IDC_EDIT_ROOM);
	roomCtrl->GetWindowText(_room);

	CEdit* uriCtrl = (CEdit*)GetDlgItem(IDC_EDIT_URI);
	uriCtrl->GetWindowText(_uri);

	if (_name.GetLength() > 0 && _room.GetLength() > 0 && _uri.GetLength() > 0) {
		EndDialog(0);
	}
	else {
		MessageBox("请填写昵称、房间号、服务器地址！");
	}



}
