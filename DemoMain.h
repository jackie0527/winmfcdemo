﻿
// DemoMain.h: 头文件
//

#pragma once
#include "DemoLogin.h"

#define WM_UPDATE_STATIC (WM_USER + 100) 
#include <map>
#include <iostream>



// DemoMain 对话框
class DemoMain : public CDialogEx
{
	// 构造
public:
	DemoMain(CWnd* pParent = nullptr);	// 标准构造函数

	void localCallback(const unsigned char* image, int width, int height, const char* userId, const char* userName, const char* room);
	void remoteCallback(const unsigned char* image, int width, int height, const char* userId, const char* userName, const char* room);
	void screenCallback(const unsigned char* image, int width, int height, const char* userId, const char* userName, const char* room);
	void statusCallback(const char* userId, const char* userName, const char* room, int online);
	void shareCallback(const char* userId, const char* userName, const char* room, int online);
	void chatCallback(const char* userId, const char* userName, const char* room, const char* context, const char* time);
	void errorCallback(const int code, const std::string msg);

	HWND wnd_;
	bool localSize;
	bool remoteSize;
	bool isShare;

	int rcRight = 320;
	int rcBottom = 240;

	std::map<std::string, CStatic*> mapCtrl_;
	std::map<std::string, int> mapItem_;
	std::map<std::string, int> mapOfflineItem_;
	std::map<std::string, BITMAPINFO> mapBit_;
	int bun1idStatic_ = 18960;

	BITMAPINFO bmiScreen;

	CCriticalSection g_clsCriticalSection;

	// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEMO_MAIN };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg LRESULT OnUpdateStatic(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()


private:
	CString _uid;
	CString _name;
	CString _room;
	CString _uri;
	bool _argee;
	DemoLogin _login;
public:
	afx_msg void OnBnClickedButtonSend();
	afx_msg void OnBnClickedButtonShare();
	virtual void OnCancel();
};
