﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 WinMFCDemo.rc 使用
//
#define IDD_WINMFCDEMO_DIALOG           102
#define IDD_DEMO_MAIN                   102
#define IDR_MAINFRAME                   128
#define IDD_DEMO_LOGIN                  130
#define IDC_EDIT_NAME                   1000
#define IDC_EDIT_URI                    1001
#define IDC_EDIT_ROOM                   1002
#define IDC_BUTTON_SUBMIT               1003
#define IDC_RADIO_ARGEE                 1004
#define IDC_STATIC_TITLE                1005
#define IDC_BUTTON_SHARE                1006
#define IDC_LIST1                       1007
#define IDC_LIST_CHAT                   1007
#define IDC_EDIT_MSG                    1008
#define IDC_BUTTON_SEND                 1009
#define IDC_STATIC_SCREEN               1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
